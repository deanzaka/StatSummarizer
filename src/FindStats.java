import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FindStats {
	boolean newData, catchData;
	String combine = null;
	public FindStats() {
		
	}
	
	public String findStats(String filename) {
		String line = null;
		
		BufferedReader bufferedReader = null;
		try {
			FileReader fileReader = new FileReader(filename);
			bufferedReader = new BufferedReader(fileReader);
			
			catchData = false;
			while((line = bufferedReader.readLine()) != null) {
				Pattern pattern = Pattern.compile("(.*,\\stotal=)(.*)(,\\ssuccess=)(.*)(,\\sfailure=)(.*)");
				Matcher matcher = pattern.matcher(line);
				if (matcher.find()) {
			    	combine=matcher.group(2)+";"+matcher.group(4)+";"+matcher.group(6)+";";
				}
				pattern = Pattern.compile("(.*,\\d*,\\sduration=)(.*)(\\sms,\\sthroughput=)(.*)(\\sTPS)");
				matcher = pattern.matcher(line);
				if (matcher.find()) {
			    	combine=combine+matcher.group(2)+";"+matcher.group(4);
				}
			}
			
		} catch (FileNotFoundException ex) {
			System.out.println("File " + filename + " not found.");
		} catch (IOException ex) {
			System.out.println("Error reading file");
		} finally {
			try {
				if(bufferedReader != null) {
					bufferedReader.close();
				}
			} catch (IOException e) {
				
			}
		}
		
		return combine;
	}
}
