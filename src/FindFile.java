import java.io.File;
import java.util.ArrayList;

public class FindFile {
	ArrayList<String> list = new ArrayList<String>();
	public FindFile() {
		
	}
	
	
	
	public ArrayList<String> findFile(String dir, String search) {
      
      File f = null;
      File[] paths;
      
      
      try{      
         // create new file
         f = new File(dir);
         
         // returns pathnames for files and directory
         paths = f.listFiles();
         
         // for each pathname in pathname array
         for(File path:paths)
         {
            // prints file and directory paths
            if(path.isDirectory()) findFile(path.toString(), search);
        	if(path.toString().contains(search))
        	{
        		list.add(path.toString());
        	}
         }
      }catch(Exception e){
         // if any error occurs
         e.printStackTrace();
      }
      return list;
   }
}
