import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class StatSummarizer {


	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		FindFile fFiles = new FindFile();
		FindStats  fStats = new FindStats();
		list = fFiles.findFile("C:\\Users\\IBM_ADMIN\\Documents\\temp\\DELTA_OUTPUT", "batchLoadStatistics.out");
		
		BufferedWriter bufferedWriter = null;
		try {
			FileWriter fileWriter = new FileWriter("C:\\Users\\IBM_ADMIN\\Documents\\temp\\DELTA_OUTPUT\\generateStats.csv");
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write("sep=;");
			bufferedWriter.newLine();
			bufferedWriter.write("Filename;Total;Success;Failure;Duration(ms);throughput;");
			bufferedWriter.newLine();
			for (String filename : list) {
				System.out.println(filename+";"+fStats.findStats(filename));
				bufferedWriter.write(filename+";"+fStats.findStats(filename));
				bufferedWriter.newLine();
				
			}
		} catch (IOException e) {
			System.out.println("Error reading file");
		} finally {
			try {
				if(bufferedWriter != null) {
					bufferedWriter.close();
				}
			} catch (IOException e){
				
			}
		}
		
	}

}
